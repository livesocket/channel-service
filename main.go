package main

import (
	"log"
	"os"
	"os/signal"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/livesocket/channel-service/actions"
	"gitlab.com/livesocket/channel-service/migrations"
	"gitlab.com/livesocket/channel-service/supers"
	"gitlab.com/livesocket/service/v2"
)

func main() {
	s := &service.Service{}
	close := s.Init(service.Actions{
		"private.channel.create":   actions.Create(s),
		"private.channel.delete":   actions.Delete(s),
		"private.channel.destroy":  actions.Destroy(s),
		"private.channel.get":      actions.Get(s),
		"private.channel.ignore":   actions.Ignore(s),
		"private.channel.unignore": actions.Unignore(s),
		"private.channel.update":   actions.Update(s),
		"super.channel":            supers.Channel(s),
		"super.ignore":             supers.Ignore(s),
	}, nil, "__channel_service", migrations.CreateChannelTable)
	defer close()

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-s.Done():
		log.Print("Router gone, exiting")
		return
	}
}
