package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/v2"
)

// EmitChannelDestroyed Emits "event.channel.destroyed"
func EmitChannelDestroyed(service *service.Service, name string) error {
	return service.Publish("event.channel.destroyed", nil, wamp.List{name}, nil)
}
