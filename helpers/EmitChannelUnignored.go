package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/v2"
)

// EmitChannelUnignored Emits "event.channel.unignored"
func EmitChannelUnignored(service *service.Service, channel string) error {
	return service.Publish("event.channel.unignored", nil, wamp.List{channel}, nil)
}
