package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/channel-service/models"
	"gitlab.com/livesocket/service/v2"
)

// EmitChannelUpdated Emits "event.channel.updated"
func EmitChannelUpdated(service *service.Service, old *models.Channel, new *models.Channel) error {
	return service.Publish("event.channel.updated", nil, wamp.List{old, new}, nil)
}
