package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/channel-service/models"
	"gitlab.com/livesocket/service/v2"
)

// EmitChannelCreated Emits "event.channel.created"
func EmitChannelCreated(service *service.Service, channel *models.Channel) error {
	return service.Publish("event.channel.created", nil, wamp.List{channel}, nil)
}
