package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/v2"
)

// EmitChannelDeleted Emits "event.channel.deleted"
func EmitChannelDeleted(service *service.Service, name string) error {
	return service.Publish("event.channel.deleted", nil, wamp.List{name}, nil)
}
