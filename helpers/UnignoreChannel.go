package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/v2"
)

// UnignoreChannel WAMP call helper for unignoring a channel
func UnignoreChannel(service *service.Service, name string) error {
	// Call get channel by name endpoint
	_, err := service.SimpleCall("private.channel.unignore", nil, wamp.Dict{"name": name})
	return err
}
