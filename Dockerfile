FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/channel-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/channel-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o channel-service

FROM scratch as release
COPY --from=builder /repos/channel-service/channel-service /channel-service
EXPOSE 8080
ENTRYPOINT ["/channel-service"]

